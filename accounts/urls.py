from django.urls import path
from .views import create_account, logout_view, create_signup


urlpatterns = [
    path("login/", create_account, name="login"),
    path("logout/", logout_view, name="logout"),
    path("signup/", create_signup, name="signup"),
]
