from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from .forms import AccountForm, SignUpForm


# Create your views here.
# LoginView
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                # redirect to list of projects
                return redirect("home")
    else:
        form = AccountForm()

    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


# Logout View
def logout_view(request):
    logout(request)
    return redirect("login")


# SignUp View
def create_signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            confirm = form.cleaned_data["password_confirmation"]
            # If passwords match
            if password == confirm:
                # Create a new user with those values
                user = User.objects.create_user(username, password=password)
                # Login the user
                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error("password", "the passwords do not match")
    else:
        form = SignUpForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)
