from django.forms import ModelForm
from .models import Project


# ProjectForm
class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = [
            "name",
            "description",
            "owner",
        ]

        def __str__(self):
            return self.name
