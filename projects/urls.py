from django.urls import path
from .views import show_projects, show_project_detail, create_project

urlpatterns = [
    path("", show_projects, name="list_projects"),
    path("<int:id>/", show_project_detail, name="show_project"),
    path("create/", create_project, name="create_project"),
]
